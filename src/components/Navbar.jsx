import React from "react";

const Navbar = () => {
    return (
      <nav className="nav">
        <div className="nav__item">
          <a className="nav__link">Профиль</a>
        </div>
        <div className="nav__item">
          <a className="nav__link">Сообщения</a>
        </div>
        <div className="nav__item">
          <a className="nav__link">Новости</a>
        </div>
        <div className="nav__item">
          <a className="nav__link">Музыка</a>
        </div>
        <div className="nav__item">
          <a className="nav__link">Настройки</a>
        </div>
      </nav>
    );
}

export default Navbar;