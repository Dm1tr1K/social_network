import React from "react";

const Header = () => {
    return (
      <header className="header">
        <img className='header__img' src={process.env.PUBLIC_URL + "/img/Logo.png"} />
      </header>
    );
}

export default Header;